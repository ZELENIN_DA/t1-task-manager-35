package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ru.t1.dzelenin.tm.api.service.ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getServerHost();

}

