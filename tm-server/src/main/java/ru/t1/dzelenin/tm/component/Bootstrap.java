package ru.t1.dzelenin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.endpoint.*;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.ISessionRepository;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.repository.IUserRepository;
import ru.t1.dzelenin.tm.api.service.*;
import ru.t1.dzelenin.tm.endpoint.*;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.repository.ProjectRepository;
import ru.t1.dzelenin.tm.repository.SessionRepository;
import ru.t1.dzelenin.tm.repository.TaskRepository;
import ru.t1.dzelenin.tm.repository.UserRepository;
import ru.t1.dzelenin.tm.service.*;
import ru.t1.dzelenin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);


    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    public void start() {
        loggerService.info("** WELCOME TO TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initPID();
        initDemoData();
    }

    public void stop() {
        loggerService.info("** TASK MANAGER HAS DONE **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = String.format("http://%s:%s/%s?WSDL",
                propertyService.getServerHost(),
                propertyService.getServerPort(), name);
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initDemoData() {
        userService.create("1", "1", "test@test.ru");
        userService.create("2", "2", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("1", Status.IN_PROGRESS));
        projectService.add(new Project("2", Status.NOT_STARTED));
        projectService.add(new Project("3", Status.IN_PROGRESS));
        projectService.add(new Project("4", Status.COMPLETED));

        taskService.create("1", "1");
        taskService.create("2", "2");
    }

}



